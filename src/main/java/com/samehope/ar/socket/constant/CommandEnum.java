package com.samehope.ar.socket.constant;

import lombok.Getter;

/**
 * @Description: 指令枚举
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Getter
public enum CommandEnum {
    ERROR(500, "命令交互失败的状态码"),
    MESSAGE(10000, "消息推送指令, 服务端不会接收处理该指令"),
    CONNECT(10001, "连接指令, 客户端连接上服务端是发送"),
    HEART(10002, "心跳指令, 客户端用于保持和服务端连接时发送"),
    TEMP_DEVICES(10003, "客户端获取在线设备客户端的指令, 服务端提供数据"),
    CONNECT_DEVICE(10004, "客户端发起连接设备客户端指令, 服务端负责中转"),
    CONNECTED_DEVICES(10005, "客户端获取在线设备客户端的指令, 服务端提供数据"),
    JOIN_ROOM(10006, "客户端端邀请已连接设备客户端加入音视频房间指令, 服务端负责中转"),
    DEVICE_OFFLINE(10007, "连接的设备已经离线, 通知手机客户端"),
    ;
    private Integer code;

    private String message;

    CommandEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
