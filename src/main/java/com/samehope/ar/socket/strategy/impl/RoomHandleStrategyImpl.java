package com.samehope.ar.socket.strategy.impl;

import com.samehope.ar.socket.annotation.Command;
import com.samehope.ar.socket.constant.CommandEnum;
import com.samehope.ar.socket.strategy.CommandHandleStrategy;
import com.samehope.ar.socket.support.MessageBody;
import com.samehope.ar.socket.support.NettyContext;
import io.netty.channel.Channel;

/**
 * @Description: 邀请在线设备加入音视频房间的指令
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Command(value = CommandEnum.JOIN_ROOM)
public class RoomHandleStrategyImpl implements CommandHandleStrategy {

    @Override
    public void handle(MessageBody messageBody, Channel channel) {
        // 获取设备号 和 房间号
        String body = String.valueOf(messageBody.getBody());

        String[] strings = body.split(":");
        if (strings.length != 2) {
            NettyContext.writeMsgAndClose(channel, "错误的消息体: " + body);
            return;
        }

        // 获取设备连接
        Channel deviceChannel = NettyContext.getConnectedChannel(strings[0]);
        String roomId = strings[1];

        // 发送指令给设备
        NettyContext.writeMsg(deviceChannel, MessageBody.of(CommandEnum.JOIN_ROOM, roomId).toString());
    }
}
