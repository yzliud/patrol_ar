package com.samehope.ar.socket.strategy.impl;

import com.samehope.ar.entity.Device;
import com.samehope.ar.entity.SysUser;
import com.samehope.ar.socket.annotation.Command;
import com.samehope.ar.socket.constant.CommandEnum;
import com.samehope.ar.socket.handler.HttpRequestHandler;
import com.samehope.ar.socket.strategy.CommandHandleStrategy;
import com.samehope.ar.socket.support.MessageBody;
import com.samehope.ar.socket.support.NettyContext;
import io.netty.channel.Channel;


/**
 * @Description: 连接指令 相当于ping pong
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Command(value = CommandEnum.CONNECT)
public class ConnectHandleStrategyImpl implements CommandHandleStrategy {

    @Override
    public void handle(MessageBody messageBody, Channel channel) {
        // 获取连接属性 用户
        SysUser user = channel.attr(HttpRequestHandler.USER).get();
        // 获取连接属性 设备
        Device device = channel.attr(HttpRequestHandler.DEVICE).get();

        if (device != null) {
            // 如果是设备, 肯定不是手机终端的连接, 需要保存临时设备信息
            NettyContext.addTempDevice(device.getDeviceMac(), channel);
        } else {
            // 如果是用户, 存储用户的连接信息
            NettyContext.addUser(user.getId(), channel);
        }

        NettyContext.writeMsg(channel, messageBody.toString());
    }
}
