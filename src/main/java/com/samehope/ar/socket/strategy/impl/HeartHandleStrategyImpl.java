package com.samehope.ar.socket.strategy.impl;

import com.samehope.ar.socket.annotation.Command;
import com.samehope.ar.socket.constant.CommandEnum;
import com.samehope.ar.socket.strategy.CommandHandleStrategy;
import com.samehope.ar.socket.support.MessageBody;
import com.samehope.ar.socket.support.NettyContext;
import io.netty.channel.Channel;

/**
 * @Description: 心跳指令处理
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Command(value = CommandEnum.HEART)
public class HeartHandleStrategyImpl implements CommandHandleStrategy {

    @Override
    public void handle(MessageBody messageBody, Channel channel) {
        NettyContext.writeMsg(channel, messageBody.toString());
    }
}
