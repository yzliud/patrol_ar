package com.samehope.ar.socket.strategy.impl;

import com.samehope.ar.socket.annotation.Command;
import com.samehope.ar.socket.constant.CommandEnum;
import com.samehope.ar.socket.strategy.CommandHandleStrategy;
import com.samehope.ar.socket.support.DeviceVO;
import com.samehope.ar.socket.support.MessageBody;
import com.samehope.ar.socket.support.NettyContext;
import io.netty.channel.Channel;

import java.util.List;

/**
 * @Description: 用于获取预连接在线设备指令的处理
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Command(value = CommandEnum.TEMP_DEVICES)
public class TempDevicesHandleStrategyImpl implements CommandHandleStrategy {

    @Override
    public void handle(MessageBody messageBody, Channel channel) {
        // 获取在线连接的眼睛终端设备
        List<DeviceVO> tempDevices = NettyContext.getTempDevices();

        // 写回数据
        NettyContext.writeMsg(channel, MessageBody.of(CommandEnum.TEMP_DEVICES, tempDevices).toString());
    }
}
