package com.samehope.ar.socket.strategy;

import com.samehope.ar.socket.support.MessageBody;
import io.netty.channel.Channel;


/**
 * @Description: 指令处理接口
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
public interface CommandHandleStrategy {

    /**
     * 消息指令处理接口
     * @param messageBody 消息体
     * @param channel 通道
     */
    void handle(MessageBody messageBody, Channel channel);
}
