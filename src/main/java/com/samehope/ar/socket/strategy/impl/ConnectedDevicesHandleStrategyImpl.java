package com.samehope.ar.socket.strategy.impl;

import com.samehope.ar.socket.annotation.Command;
import com.samehope.ar.socket.constant.CommandEnum;
import com.samehope.ar.socket.strategy.CommandHandleStrategy;
import com.samehope.ar.socket.support.MessageBody;
import com.samehope.ar.socket.support.NettyContext;
import com.samehope.ar.socket.support.UserVO;
import io.netty.channel.Channel;

import java.util.List;

/**
 * @Description: 用于已被连接使用的用户设备列表
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Command(value = CommandEnum.CONNECTED_DEVICES)
public class ConnectedDevicesHandleStrategyImpl implements CommandHandleStrategy {

    @Override
    public void handle(MessageBody messageBody, Channel channel) {
        // 获取已连接的在线设备
        List<UserVO> onlineUsers = NettyContext.getOnlineUsers();

        // 写回数据
        NettyContext.writeMsg(channel, MessageBody.of(CommandEnum.CONNECTED_DEVICES, onlineUsers).toString());
    }
}
