package com.samehope.ar.socket.annotation;

import com.samehope.ar.socket.constant.CommandEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Description:
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Command {

    CommandEnum value();
}
