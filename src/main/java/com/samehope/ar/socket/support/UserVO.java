package com.samehope.ar.socket.support;

import lombok.Data;

/**
 * @Description:
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class UserVO {

    /**
     * 在线的用户ID
     */
    private Long userId;

    /**
     * 在线的用户姓名
     */
    private String name;

    /**
     * 用户连接的设备
     */
    private DeviceVO deviceVO;
}
