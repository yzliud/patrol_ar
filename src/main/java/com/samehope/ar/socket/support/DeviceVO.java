package com.samehope.ar.socket.support;

import lombok.Data;

/**
 * @Description: 设备视图对象
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class DeviceVO {

    private String deviceMac;

    private String deviceName;
}
