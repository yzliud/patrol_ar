package com.samehope.ar.socket.support;

import com.alibaba.fastjson.JSON;
import com.samehope.ar.socket.constant.CommandEnum;
import lombok.Data;

/**
 * @Description: 消息体类型
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class MessageBody {

    private Integer command;

    private Object body;

    public static MessageBody of(CommandEnum commandEnum, Object body) {
        MessageBody messageBody = new MessageBody();
        messageBody.setCommand(commandEnum.getCode());
        messageBody.setBody(body);
        return messageBody;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
