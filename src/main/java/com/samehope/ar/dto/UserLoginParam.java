package com.samehope.ar.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Description: 用户登录参数
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class UserLoginParam {

    @NotNull(message = "请填写用户名")
    private String mobile;

    @NotNull(message = "请填写密码")
    private String password;
}
