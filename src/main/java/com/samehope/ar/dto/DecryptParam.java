package com.samehope.ar.dto;

import lombok.Data;

/**
 * @Description:
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class DecryptParam {

    private String code;

    private String iv;

    private String encryptedData;
}
