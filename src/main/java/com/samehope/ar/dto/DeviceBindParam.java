package com.samehope.ar.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Description:
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class DeviceBindParam {

    @NotBlank(message = "设备名称不能为空")
    private String deviceName;

    @NotBlank(message = "绑定校验码不能为空")
    private String code;

    @NotNull(message = "创建人不能为空")
    private Long createBy;

}
