package com.samehope.ar.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Description: 巡检项回复参数
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class TaskItemReplyParam {

    @NotNull(message = "任务巡检项ID不能为空")
    private Long taskItemId;

    private String taskItemChildIds;

    @NotBlank(message = "回复内容不能为空")
    private String replyContent;

    private String picUrl;

    private String videoUrl;

    private String replyDesc;

    private String coordinate;

}
