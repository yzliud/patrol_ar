package com.samehope.ar.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Description: 分页查询参数
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class PaginationParam {

    @NotNull(message = "页码不能为空")
    private Integer pageNum;

    @NotNull(message = "每页显示条数不能为空")
    private Integer pageSize;
}
