package com.samehope.ar.component;

import com.alibaba.fastjson.JSON;
import com.samehope.ar.common.CommonResult;
import com.samehope.ar.util.RedisUtil;
import com.samehope.ar.util.SessionUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

/**
 * @Description: 接口限流使用, 针对单个接口
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Slf4j
public class ApiRateLimiterFilter extends OncePerRequestFilter {

    /**
     * 用户请求同一个接口的频率限制, 默认1秒内对同一个接口不允许重复请求
     * @param request
     * @param response
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        Long userId = SessionUtil.getUserId();

        if (request.getMethod().equalsIgnoreCase("options") || userId == null) {
            filterChain.doFilter(request, response);
            return;
        }
        Boolean flag = RedisUtil.setIfAbsent(userId +":" + request.getRequestURI(), LocalDateTime.now().toString(), 1L);
        if (flag) {
            filterChain.doFilter(request, response);
        } else {
            response.setHeader("Access-Control-Allow-Origin", request.getHeader("origin"));
            response.setHeader("Access-Control-Allow-Credentials", "true");
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/JSON");
            try(PrintWriter writer = response.getWriter()) {
                writer.write(JSON.toJSONString(CommonResult.failed("请求过于频繁, 请稍后再试")));
                writer.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
