package com.samehope.ar.constant;

import lombok.Getter;

/**
 * @Description: 项目状态枚举
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Getter
public enum QuestionStatusEnum {

    NEW(0, "未分配"),

    LOADING(1, "待处理"),

    OVER(2, "已结束")
    ;

    private Integer code;

    private String description;

    QuestionStatusEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }
}
