package com.samehope.ar.constant;

import lombok.Getter;

/**
 * @Description: 巡检项回复状态
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Getter
public enum ItemReplyStatusEnum {

    NORMAL(0, "正常"),

    ERROR(1, "异常"),
    ;

    private Integer code;

    private String message;

    ItemReplyStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
