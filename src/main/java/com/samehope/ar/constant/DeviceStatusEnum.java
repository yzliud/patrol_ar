package com.samehope.ar.constant;

import lombok.Getter;

/**
 * @Description: 设备绑定吗
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Getter
public enum DeviceStatusEnum {

    NOT_ACTIVE(0, "未激活"),
    ACTIVE(1, "已激活")
    ;
    private Integer code;

    private String message;

    DeviceStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
