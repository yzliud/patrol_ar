package com.samehope.ar.constant;

import lombok.Getter;

/**
 * @Description: 巡检项状态
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Getter
public enum ItemStatusEnum {

    NEW(0, "未执行"),

    LOADING(1, "执行中"),

    OVER(9, "执行结束")
    ;

    private Integer code;

    private String message;

    ItemStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
