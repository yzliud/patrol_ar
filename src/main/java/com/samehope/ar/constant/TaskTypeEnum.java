package com.samehope.ar.constant;

import lombok.Getter;

/**
 * <p>
 * 任务类型
 * </p>
 *
 * @author LD
 * @since 2020-1-6
 */
@Getter
public enum TaskTypeEnum {

    CYCLE(0, "周期任务"),
    SINGLE(1, "单次任务")
    ;

    private Integer code;

    private String description;

    TaskTypeEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }
}
