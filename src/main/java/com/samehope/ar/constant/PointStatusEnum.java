package com.samehope.ar.constant;

import lombok.Getter;

/**
 * @Description: 巡检点状态
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Getter
public enum PointStatusEnum {

    NEW(0, "未执行"),

    LOADING(1, "执行中"),

    UNCHECK_OVER(8, "未检查结束"),

    OVER(9, "执行结束")
    ;

    private Integer code;

    private String message;

    PointStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
