package com.samehope.ar.constant;

import lombok.Getter;

/**
 * @Description:
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Getter
public enum IsManageEnum {

    YES(1, "是"),
    NO(0, "否")
    ;

    private Integer code;

    private String description;

    IsManageEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }
}
