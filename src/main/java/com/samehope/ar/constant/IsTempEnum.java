package com.samehope.ar.constant;

import lombok.Getter;

/**
 * @Description:
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Getter
public enum IsTempEnum {

    NOMAL(0, "正式"),
    TEMP(1, "临时")
    ;

    private Integer code;

    private String description;

    IsTempEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }
}
