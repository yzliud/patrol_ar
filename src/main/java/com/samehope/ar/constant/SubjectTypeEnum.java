package com.samehope.ar.constant;

import lombok.Getter;

/**
 * @Description: 巡检项问题类型枚举
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Getter
public enum SubjectTypeEnum {

    GAP_FILLING(0, "填空题"),

    CHOICE(1, "选择题"),

    TRUE_OR_FALSE(2, "判断题"),

    RANGE(3, "范围题")
    ;
    private Integer code;

    private String message;

    SubjectTypeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
