package com.samehope.ar.constant;

import lombok.Getter;

/**
 * @Description: 删除标记枚举类
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Getter
public enum DelFlagEnum {

    NORMAL(0, "正常"),

    DELETED(1, "已删除")

    ;
    private Integer code;

    private String description;

    DelFlagEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }
}
