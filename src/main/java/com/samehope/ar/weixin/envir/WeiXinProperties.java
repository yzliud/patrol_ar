package com.samehope.ar.weixin.envir;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "weixin")
public class WeiXinProperties {

    private String miniAppId;

    private String miniSecretKey;

    private String publicAppId;

    private String publicSecretKey;
}
