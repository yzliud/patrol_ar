package com.samehope.ar.weixin.support;

import com.alibaba.fastjson.JSON;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * @Description: 请求结果
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
public class ApiResult {

    private Map<String, Object> attrs;

    private String json;

    public ApiResult(String jsonStr) {
        this.json = jsonStr;

        try {
            Map<String, Object> temp = JSON.parseObject(jsonStr, Map.class);
            this.attrs = temp;
        } catch (Exception var3) {
            throw new RuntimeException(var3);
        }
    }

    public static ApiResult create(String jsonStr) {
        return new ApiResult(jsonStr);
    }

    public String getJson() {
        return this.json;
    }

    public boolean isSucceed() {
        Integer errorCode = this.getErrorCode();
        return errorCode == null || errorCode == 0;
    }

    public Integer getErrorCode() {
        return this.getInt("errcode");
    }

    public String getErrorMsg() {
        Integer errorCode = this.getErrorCode();
        if (errorCode != null) {
            String result = ErrorCodeEnum.get(errorCode);
            if (result != null) {
                return result;
            }
        }

        return (String)this.attrs.get("errmsg");
    }

    public Object get(String name) {
        return this.attrs.get(name);
    }

    public String getStr(String name) {
        return (String)this.attrs.get(name);
    }

    public Integer getInt(String name) {
        Number number = (Number)this.attrs.get(name);
        return number == null ? null : number.intValue();
    }

    public Long getLong(String name) {
        Number number = (Number)this.attrs.get(name);
        return number == null ? null : number.longValue();
    }

    public BigInteger getBigInteger(String name) {
        return (BigInteger)this.attrs.get(name);
    }

    public Double getDouble(String name) {
        return (Double)this.attrs.get(name);
    }

    public BigDecimal getBigDecimal(String name) {
        return (BigDecimal)this.attrs.get(name);
    }

    public Boolean getBoolean(String name) {
        return (Boolean)this.attrs.get(name);
    }

    public List getList(String name) {
        return (List)this.attrs.get(name);
    }

    public Map getMap(String name) {
        return (Map)this.attrs.get(name);
    }

    public boolean isAccessTokenInvalid() {
        Integer ec = this.getErrorCode();
        return ec != null && (ec == 40001 || ec == 42001 || ec == 42002 || ec == 40014);
    }
}
