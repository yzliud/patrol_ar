package com.samehope.ar.weixin.support;

import lombok.Data;

/**
 * @Description: 微信小程序Session信息
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class MiniSession {

    private String openid;

    private String session_key;

    private String unionid;

    private Integer errcode;

    private String errmsg;
}
