package com.samehope.ar.weixin.support;

import lombok.Data;

/**
 * @Description: 微信公众号授权令牌
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class AccessToken {

    private String access_token;

    private Integer expires_in;

    private Integer errcode;

    private String errmsg;
}
