package com.samehope.ar.weixin.support;

import lombok.Data;

/**
 * @Description: 小程序水印
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class Watermark {

    private String appid;

    private String timestamp;
}
