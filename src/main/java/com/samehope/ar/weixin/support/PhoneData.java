package com.samehope.ar.weixin.support;

import lombok.Data;

/**
 * @Description: 手机号解密数据
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class PhoneData {

    /**
     * 带区号的手机号码
     */
    private String phoneNumber;

    /**
     * 不带区号的手机号码
     */
    private String purePhoneNumber;

    /**
     * 区号
     */
    private String countryCode;

    /**
     * 防伪水印
     */
    private Watermark watermark;
}
