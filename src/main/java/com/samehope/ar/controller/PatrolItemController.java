package com.samehope.ar.controller;


import com.samehope.ar.service.IPatrolItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 巡检项 前端控制器
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@RestController
@RequestMapping("/item")
public class PatrolItemController {

    @Autowired
    private IPatrolItemService itemService;



}
