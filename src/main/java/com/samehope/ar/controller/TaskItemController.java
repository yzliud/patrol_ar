package com.samehope.ar.controller;


import com.samehope.ar.common.CommonResult;
import com.samehope.ar.dto.TaskItemReplyParam;
import com.samehope.ar.service.ITaskItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * <p>
 * 巡检项快照 前端控制器
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@RestController
@RequestMapping("/taskItem")
public class TaskItemController {

    @Autowired
    private ITaskItemService taskItemService;

    /**
     * 回复巡检项
     * @return
     */
    @PreAuthorize("hasRole('MEMBER')")
    @RequestMapping(value = "/reply/v1", method = RequestMethod.POST)
    public CommonResult reply(@Valid @RequestBody TaskItemReplyParam param) {
        taskItemService.reply(param);
        return CommonResult.success(null);
    }

}
