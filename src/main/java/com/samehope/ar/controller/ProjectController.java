package com.samehope.ar.controller;


import com.samehope.ar.common.CommonResult;
import com.samehope.ar.service.IProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 * 项目 前端控制器
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@RestController
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    /**
     * 分页获取所有项目
     * @return
     */
    @PreAuthorize("hasRole('EMPLOYEE')")
    @RequestMapping(value = "/projects/v1", method = RequestMethod.GET)
    public CommonResult projects() {
        return CommonResult.success(projectService.projects());
    }

}
