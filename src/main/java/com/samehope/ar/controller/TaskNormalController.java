package com.samehope.ar.controller;


import com.samehope.ar.common.CommonResult;
import com.samehope.ar.service.ITaskNormalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 正式巡检任务 前端控制器
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@RestController
@RequestMapping("/taskNormal")
public class TaskNormalController {

    @Autowired
    private ITaskNormalService taskNormalService;

    /**
     * 团队成员获取当前所有任务
     * @return
     */
    @PreAuthorize("hasRole('MEMBER')")
    @RequestMapping(value = "/tasks/v1", method = RequestMethod.GET)
    public CommonResult tasks() {
        return CommonResult.success(taskNormalService.tasks());
    }

    /**
     * 团队成员获取当前待执行任务
     * @return
     */
    @PreAuthorize("hasRole('MEMBER')")
    @RequestMapping(value = "/tasksInProgress/v1", method = RequestMethod.GET)
    public CommonResult tasksInProgress() {
        return CommonResult.success(taskNormalService.tasksInProgress());
    }
}
