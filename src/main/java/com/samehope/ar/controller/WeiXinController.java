package com.samehope.ar.controller;

import com.samehope.ar.common.CommonResult;
import com.samehope.ar.dto.DecryptParam;
import com.samehope.ar.util.WeiXinMiniUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * @Description: 微信相关接口
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@RestController
@RequestMapping("/wx")
public class WeiXinController {

    /**
     * 解密数据接口
     * @param decryptParam
     * @return
     */
    @RequestMapping(value = "/phoneNumber/v1", method = RequestMethod.POST)
    public CommonResult decrypt(@RequestBody DecryptParam decryptParam) {
        return CommonResult.success(WeiXinMiniUtils.decryptPhoneData(decryptParam.getCode(),
                decryptParam.getEncryptedData(), decryptParam.getIv()));
    }
}
