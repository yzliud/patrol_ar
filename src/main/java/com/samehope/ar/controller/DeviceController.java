package com.samehope.ar.controller;


import com.samehope.ar.common.CommonResult;
import com.samehope.ar.dto.DeviceBindParam;
import com.samehope.ar.service.IDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 设备 前端控制器
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@RestController
@RequestMapping("/device")
public class DeviceController {


    @Autowired
    private IDeviceService deviceService;

    /**
     * 绑定设备
     */
    @PreAuthorize("hasRole('EMPLOYEE')")
    @RequestMapping(value = "/bind/v1", method = RequestMethod.POST)
    public CommonResult bind(@Valid @RequestBody DeviceBindParam param) {
        deviceService.bindDevice(param);
        return CommonResult.success(null);
    } 

}
