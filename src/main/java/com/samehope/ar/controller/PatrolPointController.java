package com.samehope.ar.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 * 巡检点 前端控制器
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@RestController
@RequestMapping("/point")
public class PatrolPointController {

}
