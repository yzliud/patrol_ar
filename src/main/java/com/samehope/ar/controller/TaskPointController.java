package com.samehope.ar.controller;


import com.samehope.ar.common.CommonResult;
import com.samehope.ar.service.ITaskPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 巡检点快照 前端控制器
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@RestController
@RequestMapping("/taskPoint")
public class TaskPointController {

    @Autowired
    private ITaskPointService taskPointService;

    /**
     * 根据任务查询巡检点
     * @return
     */
    @PreAuthorize("hasRole('MEMBER')")
    @RequestMapping(value = "/points/v1", method = RequestMethod.GET)
    public CommonResult points(@RequestParam Long taskId) {
        return CommonResult.success(taskPointService.points(taskId));
    }
}
