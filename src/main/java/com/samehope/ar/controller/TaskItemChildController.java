package com.samehope.ar.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 巡检项选项快照 前端控制器
 * </p>
 *
 * @author ZhangLuo
 * @since 2020-01-14
 */
@RestController
@RequestMapping("/ar/task-item-child")
public class TaskItemChildController {

}
