package com.samehope.ar.controller;


import com.samehope.ar.common.CommonResult;
import com.samehope.ar.dto.UserLoginParam;
import com.samehope.ar.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 用户 前端控制器
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@RestController
@RequestMapping("/user")
public class SysUserController {

    @Value("${jwt.tokenHeader}")
    private String tokenHeader;
    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @Autowired
    private ISysUserService userService;

    /**
     * 登录逻辑
     * @param param
     * @return
     */
    @RequestMapping(value = "/login/v1", method = RequestMethod.POST)
    public CommonResult login(@RequestBody @Valid UserLoginParam param) {
        String token = userService.login(param.getMobile(), param.getPassword());
        if (token == null) {
            return CommonResult.validateFailed("用户名或密码错误");
        }
        Map<String, String> tokenMap = new HashMap<>();
        tokenMap.put("token", token);
        tokenMap.put("tokenHead", tokenHead);
        return CommonResult.success(tokenMap);
    }

}
