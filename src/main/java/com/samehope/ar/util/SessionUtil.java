package com.samehope.ar.util;

import com.samehope.ar.bo.UserSessionBO;

/**
 * @Description: 用户信息存储类
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
public class SessionUtil {

    private static ThreadLocal<UserSessionBO> userStorage = new ThreadLocal<>();

    private static final UserSessionBO DEFAULT = new UserSessionBO();

    public static void setUser(UserSessionBO user) {
        userStorage.set(user);
    }

    /**
     * 获取用户
     * @return
     */
    public static UserSessionBO getUser() {
        return userStorage.get() == null ? DEFAULT : userStorage.get();
    }

    /**
     * 获取用户ID
     * @return
     */
    public static Long getUserId() {
        return getUser().getUserId();
    }

    /**
     * 获取当前企业ID
     * @return
     */
    public static Long getCompanyId() {
        return getUser().getCompanyId();
    }

    /**
     * 获取当前项目ID
     * @return
     */
    public static Long getProjectId() {
        return getUser().getProjectId();
    }

    /**
     * 获取使用的设备ID
     * @return
     */
    public static Long getDeviceId() {
        return getUser().getDeviceId();
    }

    /**
     * 获取使用的设备识别码
     * @return
     */
    public static String getDeviceMac() {
        return getUser().getDeviceMac();
    }
}
