package com.samehope.ar.util;

import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;

/**
 * @Description 日期格式帮助类
 * @Author ZhangLuo
 * @Email 1946430@qq.com
 */
public class DateUtils {

    private static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * 功能描述 Date 转 LocalDateTime
     * @author 垠坤-张罗
     * @date 2019-3-8 10:46
     * @param date
     * @return java.time.LocalDateTime
     */
    public static LocalDateTime toLocalDateTime(Date date) {
        if (Objects.isNull(date)) return null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String localDateTime = format.format(date);
        return toLocalDateTime(localDateTime);
    }

    /**
     * 功能描述 将字符串格式的事件转换成 LocalDateTime
     * @author 垠坤-张罗
     * @date 2019-3-8 13:47
     * @param localDateTime 格式: yyyy-MM-dd HH:mm:ss
     * @return java.time.LocalDateTime
     */
    public static LocalDateTime toLocalDateTime(String localDateTime) {
        if (StringUtils.isEmpty(localDateTime)) return null;
        return LocalDateTime.parse(localDateTime, dateTimeFormatter);
    }

    /**
     * 功能描述 将字符串格式的事件转换成 LocalDateTime
     * @author 垠坤-张罗
     * @date 2019-3-8 13:47
     * @param localDateTime 格式: 自定义
     * @return java.time.LocalDateTime
     */
    public static LocalDateTime toLocalDateTime(String localDateTime, DateTimeFormatter formatter) {
        if (StringUtils.isEmpty(localDateTime)) return null;
        return LocalDateTime.parse(localDateTime, formatter);
    }

    /**
     * 格式转换
     * @param localDateTime
     * @return
     */
    public static Date toDate(LocalDateTime localDateTime) {
        String date = toString(localDateTime);
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 功能描述 转换成 yyyy-MM-dd HH:mm:ss 格式字符串
     * @author 垠坤-张罗
     * @date 2019-3-19 15:25
     * @param localDateTime
     * @return java.lang.String
     */
    public static String toString(LocalDateTime localDateTime) {
        if (Objects.isNull(localDateTime)) return null;
        return dateTimeFormatter.format(localDateTime);
    }

    /**
     * 功能描述 自定义格式转换字符串
     * @author 垠坤-张罗
     * @date 2019-3-19 15:27
     * @param localDateTime
     * @param pattern
     * @return java.lang.String
     */
    public static String toString(LocalDateTime localDateTime, String pattern) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        return dateTimeFormatter.format(localDateTime);
    }

    /**
     * 功能描述 自定义格式转换字符串
     * @author 垠坤-张罗
     * @date 2019-3-19 15:27
     * @param date
     * @param pattern
     * @return java.lang.String
     */
    public static String toString(Date date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);
    }

    /**
     * 功能描述 判断2个日期是否是同一年的同一个月
     * @author 垠坤-张罗
     * @date 2019-3-26 17:13
     * @param firstDate
     * @param secondDate
     * @return boolean
     */
    public static boolean isSameYearAndMonth(LocalDateTime firstDate, LocalDateTime secondDate) {
        return firstDate.getYear() == secondDate.getYear()
                && firstDate.getMonthValue() == secondDate.getMonthValue();
    }

    /**
     * 功能描述 判断是否闰年
     * @author 垠坤-张罗
     * @date 2019-4-30 11:36
     * @param year
     * @return boolean
     */
    public static boolean isLeapYear(int year) {
        return (year % 100 == 0 && year % 400 == 0) || (year % 100 != 0 && year % 4 == 0);
    }
}
