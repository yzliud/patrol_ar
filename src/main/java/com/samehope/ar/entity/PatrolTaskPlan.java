package com.samehope.ar.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 巡检计划任务
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("patrol_task_plan")
public class PatrolTaskPlan implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 公司ID
     */
    private Long companyId;

    /**
     * 项目ID
     */
    private Long projectId;

    /**
     * 任务编码
     */
    private String taskNo;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 任务类型(0-周期任务  1-单次任务)
     */
    private Integer taskType;

    /**
     * 任务路线
     */
    private Long routeId;

    /**
     * 执行团队
     */
    private Long teamId;

    /**
     * 执行人员
     */
    private Long memberId;

    /**
     * 开始日期
     */
    private LocalDateTime startDate;

    /**
     * 结束日期
     */
    private LocalDateTime endDate;

    /**
     * 执行星期(1-7 ,以，分割)
     */
    private String execWeek;

    /**
     * 执行次数
     */
    private Integer execCount;

    /**
     * 每天执行开始时间
     */
    private String execStartTime;

    /**
     * 每天执行结束时间
     */
    private String execEndTime;

    /**
     * 创建者
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    /**
     * 更新者
     */
    private Long updateBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateDate;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 删除标记(0-正常 1-删除)
     */
    private Integer delFlag;


}
