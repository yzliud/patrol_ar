package com.samehope.ar.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 巡检项
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("patrol_item")
public class PatrolItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 公司ID
     */
    private Long companyId;

    /**
     * 项目ID
     */
    private Long projectId;

    /**
     * 巡检项名称
     */
    private String patrolItemName;

    /**
     * 巡检项分类ID
     */
    private Long patrolItemTypeId;

    /**
     * 巡检项编号
     */
    private String patrolItemNo;

    /**
     * 巡检项描述
     */
    private String patrolItemDesc;

    /**
     * 是否需要现场拍照
     */
    private Integer isPhoto;

    /**
     * 是否需要录制视频
     */
    private Integer isVideo;

    /**
     * 是否需要备注
     */
    private Integer isRemark;

    /**
     * 是否需要记录gps
     */
    private Integer isGps;

    /**
     * 题目类型(0-填空题 1-选择题 2-判断题  3-范围题)'
     */
    private Integer subjectType;

    /**
     * 题目
     */
    private String title;

    /**
     * 创建者
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    /**
     * 更新者
     */
    private Long updateBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateDate;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 删除标记(0-正常 1-删除)
     */
    private Integer delFlag;


}
