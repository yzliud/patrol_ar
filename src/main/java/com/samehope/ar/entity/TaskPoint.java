package com.samehope.ar.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 巡检点快照
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("task_point")
public class TaskPoint implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 公司ID
     */
    private Long companyId;

    /**
     * 所属巡检点
     */
    private Long patrolPointId;

    /**
     * 所属项目
     */
    private Long projectId;

    /**
     * 所属任务ID
     */
    private Long taskId;

    private Long taskRouteId;

    /**
     * 点位名称
     */
    private String pointName;

    /**
     * 点位编号
     */
    private String pointCode;

    /**
     * 点位位置
     */
    private String pointAddress;

    /**
     * GPS坐标
     */
    private String coordinate;

    /**
     * 二维码值
     */
    private String qrCode;

    /**
     * 分组信息
     */
    private Long pointGroup;

    /**
     * 巡检项是否必检
     */
    private Integer isMustCheck;

    /**
     * 巡检项异常等级
     */
    private Integer errorGrade;

    /**
     * 状态(0-未执行 1-执行中 9-执行结束)
     */
    private Integer taskPointStatus;

    /**
     * 执行人员
     */
    private Long execBy;

    /**
     * 执行执行开始时间
     */
    private LocalDateTime realStartDate;

    /**
     * 实际执行结束时间
     */
    private LocalDateTime realEndDate;

    /**
     * 排序
     */
    private Long sortNo;

    /**
     * 创建者
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    /**
     * 更新者
     */
    private Long updateBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateDate;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 删除标记(0-正常 1-删除)
     */
    private Integer delFlag;


}
