package com.samehope.ar.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 巡检项快照
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("task_item")
public class TaskItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 公司ID
     */
    private Long companyId;

    /**
     * 所属巡检项
     */
    private Long patrolItemId;

    /**
     * 所属项目
     */
    private Long projectId;

    /**
     * 所属任务ID
     */
    private Long taskId;

    private Long taskRouteId;

    /**
     * 所属点位
     */
    private Long taskPointId;

    /**
     * 巡检项名称
     */
    private String patrolItemName;

    /**
     * 巡检项分类
     */
    private Long patrolItemTypeId;

    /**
     * 巡检项编号
     */
    private String patrolItemNo;

    /**
     * 巡检项描述
     */
    private String patrolItemDesc;

    /**
     * 是否需要现场拍照
     */
    private Integer isPhoto;

    /**
     * 是否需要录制视频
     */
    private Integer isVideo;

    /**
     * 是否需要备注
     */
    private Integer isRemark;

    /**
     * 是否需要记录gps
     */
    private Integer isGps;

    /**
     * 题目类型(0-填空题 1-选择题 2-判断题  3-范围题)'
     */
    private Integer subjectType;

    /**
     * 题目
     */
    private String title;

    /**
     * 实际开始时间
     */
    private LocalDateTime realStartDate;

    /**
     * 实际结束时间
     */
    private LocalDateTime realEndDate;

    /**
     * 状态(0-未执行 1-执行中 9-执行结束)
     */
    private Integer taskItemStatus;

    /**
     * 执行人员
     */
    private Long execBy;

    /**
     * 排序
     */
    private Long sortNo;

    /**
     * 创建者
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    /**
     * 更新者
     */
    private Long updateBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateDate;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 删除标记(0-正常 1-删除)
     */
    private Integer delFlag;


}
