package com.samehope.ar.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 正式巡检任务
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("task_normal")
public class TaskNormal implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 公司ID
     */
    private Long companyId;

    /**
     * 项目ID
     */
    private Long projectId;

    /**
     * 计划任务ID
     */
    private Long taskPlanId;

    /**
     * 任务编码
     */
    private String taskNo;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 任务类型
     */
    private Long taskType;

    /**
     * 任务路线
     */
    private Long routeId;

    /**
     * 执行团队
     */
    private Long teamId;

    /**
     * 计划执行人员
     */
    private String planExecBys;

    /**
     * 开始日期
     */
    private LocalDateTime planStartDate;

    /**
     * 实际执行人
     */
    private Long realExecBy;

    /**
     * 结束日期
     */
    private LocalDateTime realStartDate;

    private LocalDateTime realEndDate;

    /**
     * 是否需要上传位置
     */
    private Integer isRecordLocation;

    /**
     * 状态(0-未执行 1-执行中 9-执行结束)
     */
    private Integer taskStatus;

    /**
     * 创建者
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    /**
     * 更新者
     */
    private Long updateBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateDate;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 删除标记(0-正常 1-删除)
     */
    private Integer delFlag;


}
