package com.samehope.ar.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 问题
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("problem")
public class Problem implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 公司ID
     */
    private Long companyId;

    /**
     * 标题
     */
    private String title;

    /**
     * 问题分类
     */
    private String problemType;

    /**
     * 所属项目
     */
    private Long projectId;

    /**
     * 所属任务ID
     */
    private Long taskId;

    private Long taskRouteId;

    /**
     * 所属点位
     */
    private Long taskPointId;

    /**
     * 巡检项ID
     */
    private Long taskItemId;

    /**
     * 问题描述
     */
    private String questionDesc;

    /**
     * 状态(0-待分配 1-待处理 9-处理完成)
     */
    private Integer questionStatus;

    /**
     * 处理人
     */
    private Long handBy;

    /**
     * 创建者
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    /**
     * 更新者
     */
    private Long updateBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateDate;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 删除标记(0-正常 1-删除)
     */
    private Integer delFlag;


}
