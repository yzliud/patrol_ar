package com.samehope.ar.service;

import com.samehope.ar.entity.TaskItemReply;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 巡检项回复 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ITaskItemReplyService extends IService<TaskItemReply> {

}
