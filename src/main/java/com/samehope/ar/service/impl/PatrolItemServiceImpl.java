package com.samehope.ar.service.impl;

import com.samehope.ar.entity.PatrolItem;
import com.samehope.ar.mapper.PatrolItemMapper;
import com.samehope.ar.service.IPatrolItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡检项 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class PatrolItemServiceImpl extends ServiceImpl<PatrolItemMapper, PatrolItem> implements IPatrolItemService {

}
