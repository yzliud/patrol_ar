package com.samehope.ar.service.impl;

import com.samehope.ar.entity.TaskItemChoice;
import com.samehope.ar.mapper.TaskItemChoiceMapper;
import com.samehope.ar.service.ITaskItemChoiceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡检项选项快照 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class TaskItemChoiceServiceImpl extends ServiceImpl<TaskItemChoiceMapper, TaskItemChoice> implements ITaskItemChoiceService {

}
