package com.samehope.ar.service.impl;

import com.samehope.ar.entity.PatrolRoutePoint;
import com.samehope.ar.mapper.PatrolRoutePointMapper;
import com.samehope.ar.service.IPatrolRoutePointService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡检路线点位表 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class PatrolRoutePointServiceImpl extends ServiceImpl<PatrolRoutePointMapper, PatrolRoutePoint> implements IPatrolRoutePointService {

}
