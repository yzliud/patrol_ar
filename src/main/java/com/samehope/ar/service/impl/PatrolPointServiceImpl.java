package com.samehope.ar.service.impl;

import com.samehope.ar.entity.PatrolPoint;
import com.samehope.ar.mapper.PatrolPointMapper;
import com.samehope.ar.service.IPatrolPointService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 巡检点 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class PatrolPointServiceImpl extends ServiceImpl<PatrolPointMapper, PatrolPoint> implements IPatrolPointService {


}
