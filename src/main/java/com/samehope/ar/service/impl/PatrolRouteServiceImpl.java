package com.samehope.ar.service.impl;

import com.samehope.ar.entity.PatrolRoute;
import com.samehope.ar.mapper.PatrolRouteMapper;
import com.samehope.ar.service.IPatrolRouteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡检路线 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class PatrolRouteServiceImpl extends ServiceImpl<PatrolRouteMapper, PatrolRoute> implements IPatrolRouteService {

}
