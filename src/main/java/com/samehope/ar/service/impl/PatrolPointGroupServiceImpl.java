package com.samehope.ar.service.impl;

import com.samehope.ar.entity.PatrolPointGroup;
import com.samehope.ar.mapper.PatrolPointGroupMapper;
import com.samehope.ar.service.IPatrolPointGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡检点分组表 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class PatrolPointGroupServiceImpl extends ServiceImpl<PatrolPointGroupMapper, PatrolPointGroup> implements IPatrolPointGroupService {

}
