package com.samehope.ar.service.impl;

import com.samehope.ar.entity.TaskItemChild;
import com.samehope.ar.mapper.TaskItemChildMapper;
import com.samehope.ar.service.ITaskItemChildService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡检项选项快照 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2020-01-14
 */
@Service
public class TaskItemChildServiceImpl extends ServiceImpl<TaskItemChildMapper, TaskItemChild> implements ITaskItemChildService {

    /**
     * 根据巡检项和ID查询子项
     * @param taskItemId
     * @param taskItemChildId
     * @return
     */
    @Override
    public TaskItemChild getByItemAndChild(Long taskItemId, Long taskItemChildId) {
        return this.baseMapper.findByItemAndChild(taskItemId, taskItemChildId);
    }
}
