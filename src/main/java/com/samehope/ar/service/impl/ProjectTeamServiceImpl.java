package com.samehope.ar.service.impl;

import com.samehope.ar.entity.ProjectTeam;
import com.samehope.ar.mapper.ProjectTeamMapper;
import com.samehope.ar.service.IProjectTeamService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目团队 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class ProjectTeamServiceImpl extends ServiceImpl<ProjectTeamMapper, ProjectTeam> implements IProjectTeamService {

}
