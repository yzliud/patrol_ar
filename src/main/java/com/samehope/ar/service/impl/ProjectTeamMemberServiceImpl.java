package com.samehope.ar.service.impl;

import com.samehope.ar.entity.ProjectTeamMember;
import com.samehope.ar.mapper.ProjectTeamMemberMapper;
import com.samehope.ar.service.IProjectTeamMemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目团队成员 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class ProjectTeamMemberServiceImpl extends ServiceImpl<ProjectTeamMemberMapper, ProjectTeamMember> implements IProjectTeamMemberService {

}
