package com.samehope.ar.service.impl;

import com.samehope.ar.entity.DeviceRecord;
import com.samehope.ar.mapper.DeviceRecordMapper;
import com.samehope.ar.service.IDeviceRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 设备使用记录 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class DeviceRecordServiceImpl extends ServiceImpl<DeviceRecordMapper, DeviceRecord> implements IDeviceRecordService {

}
