package com.samehope.ar.service.impl;

import com.samehope.ar.entity.PatrolItemType;
import com.samehope.ar.mapper.PatrolItemTypeMapper;
import com.samehope.ar.service.IPatrolItemTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡检项分类表 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class PatrolItemTypeServiceImpl extends ServiceImpl<PatrolItemTypeMapper, PatrolItemType> implements IPatrolItemTypeService {

}
