package com.samehope.ar.service.impl;

import com.samehope.ar.entity.SysUserCompany;
import com.samehope.ar.mapper.SysUserCompanyMapper;
import com.samehope.ar.service.ISysUserCompanyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户公司 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class SysUserCompanyServiceImpl extends ServiceImpl<SysUserCompanyMapper, SysUserCompany> implements ISysUserCompanyService {

}
