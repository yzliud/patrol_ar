package com.samehope.ar.service.impl;

import com.samehope.ar.entity.PatrolPointDetail;
import com.samehope.ar.mapper.PatrolPointDetailMapper;
import com.samehope.ar.service.IPatrolPointDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡检点项目明细 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class PatrolPointDetailServiceImpl extends ServiceImpl<PatrolPointDetailMapper, PatrolPointDetail> implements IPatrolPointDetailService {

}
