package com.samehope.ar.service.impl;

import com.samehope.ar.entity.TaskPoint;
import com.samehope.ar.mapper.TaskPointMapper;
import com.samehope.ar.service.ITaskPointService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.samehope.ar.util.SessionUtil;
import com.samehope.ar.vo.PointDetailVO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 巡检点快照 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class TaskPointServiceImpl extends ServiceImpl<TaskPointMapper, TaskPoint> implements ITaskPointService {

    /**
     * 获取巡检点明细
     * @param taskId
     * @return
     */
    @Override
    public List<PointDetailVO> points(Long taskId) {
        return this.baseMapper.findPointsByProjectAndTask(SessionUtil.getProjectId(), taskId);
    }
}
