package com.samehope.ar.service.impl;

import com.samehope.ar.entity.CompanyConfig;
import com.samehope.ar.mapper.CompanyConfigMapper;
import com.samehope.ar.service.ICompanyConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公司配置参数 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class CompanyConfigServiceImpl extends ServiceImpl<CompanyConfigMapper, CompanyConfig> implements ICompanyConfigService {

}
