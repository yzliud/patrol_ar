package com.samehope.ar.service.impl;

import com.samehope.ar.entity.PatrolItemChild;
import com.samehope.ar.mapper.PatrolItemChildMapper;
import com.samehope.ar.service.IPatrolItemChildService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡检项子表 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class PatrolItemChildServiceImpl extends ServiceImpl<PatrolItemChildMapper, PatrolItemChild> implements IPatrolItemChildService {

}
