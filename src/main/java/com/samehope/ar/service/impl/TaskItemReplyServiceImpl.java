package com.samehope.ar.service.impl;

import com.samehope.ar.entity.TaskItemReply;
import com.samehope.ar.mapper.TaskItemReplyMapper;
import com.samehope.ar.service.ITaskItemReplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡检项回复 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class TaskItemReplyServiceImpl extends ServiceImpl<TaskItemReplyMapper, TaskItemReply> implements ITaskItemReplyService {

}
