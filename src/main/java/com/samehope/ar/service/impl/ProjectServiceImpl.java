package com.samehope.ar.service.impl;

import com.samehope.ar.entity.Project;
import com.samehope.ar.mapper.ProjectMapper;
import com.samehope.ar.service.IProjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.samehope.ar.util.SessionUtil;
import com.samehope.ar.vo.ProjectListVO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 项目 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class ProjectServiceImpl extends ServiceImpl<ProjectMapper, Project> implements IProjectService {

    /**
     * 分页获取所有项目
     * @return
     */
    @Override
    public List<ProjectListVO> projects() {
        return this.baseMapper.getProjects(SessionUtil.getCompanyId(), SessionUtil.getUserId());
    }
}
