package com.samehope.ar.service.impl;

import com.samehope.ar.entity.PatrolTaskPlan;
import com.samehope.ar.mapper.PatrolTaskPlanMapper;
import com.samehope.ar.service.IPatrolTaskPlanService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡检计划任务 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class PatrolTaskPlanServiceImpl extends ServiceImpl<PatrolTaskPlanMapper, PatrolTaskPlan> implements IPatrolTaskPlanService {

}
