package com.samehope.ar.service.impl;

import com.samehope.ar.entity.ProblemReply;
import com.samehope.ar.mapper.ProblemReplyMapper;
import com.samehope.ar.service.IProblemReplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 问题处理 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class ProblemReplyServiceImpl extends ServiceImpl<ProblemReplyMapper, ProblemReply> implements IProblemReplyService {

}
