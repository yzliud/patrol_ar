package com.samehope.ar.service.impl;

import com.samehope.ar.entity.TaskNormal;
import com.samehope.ar.mapper.TaskNormalMapper;
import com.samehope.ar.service.ITaskNormalService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.samehope.ar.util.SessionUtil;
import com.samehope.ar.vo.TaskListVO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 正式巡检任务 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class TaskNormalServiceImpl extends ServiceImpl<TaskNormalMapper, TaskNormal> implements ITaskNormalService {

    /**
     * 团队成员获取任务
     * @return
     */
    @Override
    public List<TaskListVO> tasks() {
        List<TaskListVO> tasks = this.baseMapper.getTasksByProjectAndUser(SessionUtil.getProjectId(), SessionUtil.getUserId());
        return tasks;
    }

    /**
     * 当前待执行的任务
     * @return
     */
    @Override
    public List<TaskListVO> tasksInProgress() {
        return this.baseMapper.getTasksInProgressByProjectAndUser(SessionUtil.getProjectId(), SessionUtil.getUserId());
    }
}
