package com.samehope.ar.service.impl;

import com.samehope.ar.entity.TaskRoute;
import com.samehope.ar.mapper.TaskRouteMapper;
import com.samehope.ar.service.ITaskRouteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡检路线快照 服务实现类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
@Service
public class TaskRouteServiceImpl extends ServiceImpl<TaskRouteMapper, TaskRoute> implements ITaskRouteService {

}
