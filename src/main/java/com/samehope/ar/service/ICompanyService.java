package com.samehope.ar.service;

import com.samehope.ar.entity.Company;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 公司 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ICompanyService extends IService<Company> {

}
