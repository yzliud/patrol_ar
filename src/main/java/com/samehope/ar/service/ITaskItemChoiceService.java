package com.samehope.ar.service;

import com.samehope.ar.entity.TaskItemChoice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 巡检项选项快照 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ITaskItemChoiceService extends IService<TaskItemChoice> {

}
