package com.samehope.ar.service;

import com.samehope.ar.entity.SysDict;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ISysDictService extends IService<SysDict> {

}
