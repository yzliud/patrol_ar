package com.samehope.ar.service;

import com.samehope.ar.entity.TaskRoute;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 巡检路线快照 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ITaskRouteService extends IService<TaskRoute> {

}
