package com.samehope.ar.service;

import com.samehope.ar.entity.PatrolItemType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 巡检项分类表 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface IPatrolItemTypeService extends IService<PatrolItemType> {

}
