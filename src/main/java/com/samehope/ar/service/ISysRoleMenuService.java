package com.samehope.ar.service;

import com.samehope.ar.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色菜单 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

}
