package com.samehope.ar.service;

import com.samehope.ar.entity.Project;
import com.baomidou.mybatisplus.extension.service.IService;
import com.samehope.ar.vo.ProjectListVO;

import java.util.List;

/**
 * <p>
 * 项目 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface IProjectService extends IService<Project> {

    /**
     * 分页获取所有项目
     * @return
     */
    List<ProjectListVO> projects();
}
