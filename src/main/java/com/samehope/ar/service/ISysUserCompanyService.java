package com.samehope.ar.service;

import com.samehope.ar.entity.SysUserCompany;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户公司 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ISysUserCompanyService extends IService<SysUserCompany> {

}
