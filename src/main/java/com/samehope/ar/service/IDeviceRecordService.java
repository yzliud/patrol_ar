package com.samehope.ar.service;

import com.samehope.ar.entity.DeviceRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 设备使用记录 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface IDeviceRecordService extends IService<DeviceRecord> {

}
