package com.samehope.ar.service;

import com.samehope.ar.dto.DeviceBindParam;
import com.samehope.ar.entity.Device;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 设备 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface IDeviceService extends IService<Device> {

    /**
     * 绑定设备
     * @param param
     */
    void bindDevice(DeviceBindParam param);

    /**
     * 根据识别码查找设备
     * @param deviceMac
     * @return
     */
    Device findByDeviceMac(String deviceMac);
}
