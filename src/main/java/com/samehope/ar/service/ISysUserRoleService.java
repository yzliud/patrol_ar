package com.samehope.ar.service;

import com.samehope.ar.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户项目角色 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
