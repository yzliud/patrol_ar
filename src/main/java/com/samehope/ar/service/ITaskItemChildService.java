package com.samehope.ar.service;

import com.samehope.ar.entity.TaskItemChild;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 巡检项选项快照 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2020-01-14
 */
public interface ITaskItemChildService extends IService<TaskItemChild> {

    /**
     * 根据巡检项和ID查询子项
     * @param taskItemId
     * @param taskItemChildId
     * @return
     */
    TaskItemChild getByItemAndChild(Long taskItemId, Long taskItemChildId);
}
