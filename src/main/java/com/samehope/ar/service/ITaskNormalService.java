package com.samehope.ar.service;

import com.samehope.ar.entity.TaskNormal;
import com.baomidou.mybatisplus.extension.service.IService;
import com.samehope.ar.vo.TaskListVO;

import java.util.List;

/**
 * <p>
 * 正式巡检任务 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ITaskNormalService extends IService<TaskNormal> {

    /**
     * 团队成员获取任务
     * @return
     */
    List<TaskListVO> tasks();

    /**
     * 当前待执行的任务
     * @return
     */
    List<TaskListVO> tasksInProgress();
}
