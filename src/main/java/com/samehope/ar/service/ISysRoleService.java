package com.samehope.ar.service;

import com.samehope.ar.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ISysRoleService extends IService<SysRole> {

}
