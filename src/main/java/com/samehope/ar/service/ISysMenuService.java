package com.samehope.ar.service;

import com.samehope.ar.entity.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜单 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ISysMenuService extends IService<SysMenu> {

}
