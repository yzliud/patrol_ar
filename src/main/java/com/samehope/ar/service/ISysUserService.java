package com.samehope.ar.service;

import com.samehope.ar.entity.SysRole;
import com.samehope.ar.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ISysUserService extends IService<SysUser> {

    /**
     * 根据手机号码查找用户
     * @param mobile
     * @return
     */
    SysUser getByMobile(String mobile);

    /**
     * 根据用户查找用户角色
     * @param id
     * @return
     */
    List<SysRole> getRoleListByUser(Long id);

    /**
     * 用户登录
     * @param mobile
     * @param password
     * @return
     */
    String login(String mobile, String password);
}
