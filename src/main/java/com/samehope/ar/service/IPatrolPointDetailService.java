package com.samehope.ar.service;

import com.samehope.ar.entity.PatrolPointDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 巡检点项目明细 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface IPatrolPointDetailService extends IService<PatrolPointDetail> {

}
