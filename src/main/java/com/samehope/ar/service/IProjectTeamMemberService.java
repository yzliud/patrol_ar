package com.samehope.ar.service;

import com.samehope.ar.entity.ProjectTeamMember;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 项目团队成员 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface IProjectTeamMemberService extends IService<ProjectTeamMember> {

}
