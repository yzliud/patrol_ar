package com.samehope.ar.service;

import com.samehope.ar.entity.CompanyConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 公司配置参数 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ICompanyConfigService extends IService<CompanyConfig> {

}
