package com.samehope.ar.service;

import com.samehope.ar.entity.Problem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 问题 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface IProblemService extends IService<Problem> {

}
