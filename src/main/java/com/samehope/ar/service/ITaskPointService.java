package com.samehope.ar.service;

import com.samehope.ar.entity.TaskPoint;
import com.baomidou.mybatisplus.extension.service.IService;
import com.samehope.ar.vo.PointDetailVO;

import java.util.List;

/**
 * <p>
 * 巡检点快照 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ITaskPointService extends IService<TaskPoint> {

    /**
     * 获取巡检点明细
     * @param taskId
     * @return
     */
    List<PointDetailVO> points(Long taskId);
}
