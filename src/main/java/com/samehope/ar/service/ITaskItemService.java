package com.samehope.ar.service;

import com.samehope.ar.dto.TaskItemReplyParam;
import com.samehope.ar.entity.TaskItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 巡检项快照 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ITaskItemService extends IService<TaskItem> {

    /**
     * 回复巡检项
     * @param param
     */
    void reply(TaskItemReplyParam param);
}
