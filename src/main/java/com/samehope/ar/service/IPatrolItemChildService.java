package com.samehope.ar.service;

import com.samehope.ar.entity.PatrolItemChild;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 巡检项子表 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface IPatrolItemChildService extends IService<PatrolItemChild> {

}
