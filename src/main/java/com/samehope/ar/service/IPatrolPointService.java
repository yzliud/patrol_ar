package com.samehope.ar.service;

import com.samehope.ar.entity.PatrolPoint;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * <p>
 * 巡检点 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface IPatrolPointService extends IService<PatrolPoint> {


}
