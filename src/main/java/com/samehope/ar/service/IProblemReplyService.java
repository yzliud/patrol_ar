package com.samehope.ar.service;

import com.samehope.ar.entity.ProblemReply;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 问题处理 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface IProblemReplyService extends IService<ProblemReply> {

}
