package com.samehope.ar.service;

import com.samehope.ar.entity.PatrolTaskPlan;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 巡检计划任务 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface IPatrolTaskPlanService extends IService<PatrolTaskPlan> {

}
