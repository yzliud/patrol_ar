package com.samehope.ar.service;

import com.samehope.ar.entity.ProjectTeam;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 项目团队 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface IProjectTeamService extends IService<ProjectTeam> {

}
