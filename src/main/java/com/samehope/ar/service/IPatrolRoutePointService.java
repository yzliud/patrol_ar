package com.samehope.ar.service;

import com.samehope.ar.entity.PatrolRoutePoint;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 巡检路线点位表 服务类
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface IPatrolRoutePointService extends IService<PatrolRoutePoint> {

}
