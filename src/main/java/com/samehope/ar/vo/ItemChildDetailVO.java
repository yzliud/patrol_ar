package com.samehope.ar.vo;

import lombok.Data;

/**
 * @Description: 巡检项子项视图对象
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class ItemChildDetailVO {

    private Long taskItemChildId;

    private String choiceItem;

}