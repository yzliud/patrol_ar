package com.samehope.ar.vo;

import lombok.Data;

/**
 * @Description: 所有项目页面视图对象
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class ProjectListVO {

    /**
     * 项目ID
     */
    private Long projectId;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 名称第一个字
     */
    private String firstChar;

    /**
     * 地址
     */
    private String address;

    /**
     * 项目描述
     */
    private String projectDesc;

    /**
     * 待执行任务数量
     */
    private Integer performTaskCount;

    /**
     * 所有的任务数量
     */
    private Integer allTaskCount;
}
