package com.samehope.ar.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

import java.util.List;

/**
 * @Description: 分页数据
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class PaginationDataVO<T> {

    private Long total;

    private Long pageNum;

    private Long pageSize;

    private List<T> data;

    public static <T> PaginationDataVO<T> of(Page<T> page) {
        PaginationDataVO<T> var = new PaginationDataVO<>();
        var.setTotal(page.getTotal());
        var.setPageNum(page.getCurrent());
        var.setPageSize(page.getSize());
        var.setData(page.getRecords());
        return var;
    }
}
