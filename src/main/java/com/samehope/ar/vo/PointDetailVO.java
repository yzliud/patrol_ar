package com.samehope.ar.vo;

import lombok.Data;

import java.util.List;

/**
 * @Description: 巡检点视图对象
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class PointDetailVO {

    private Long taskPointId;

    private String pointName;

    private String pointCode;

    private String pointAddress;

    private String coordinate;

    private String pointStatus;

    private Integer isMustCheck;

    private List<ItemDetailVO> items;
}
