package com.samehope.ar.vo;

import lombok.Data;

/**
 * @Description: 获取任务视图对象
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class TaskListVO {

    /**
     * 正式任务ID
     */
    private Long taskNormalId;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 任务名称首字
     */
    private String firstChar;

    /**
     * 计划执行时间
     */
    private String time;

    /**
     * 任务描述
     */
    private String remarks;

    /**
     * 任务类型 0 周期任务 1单次任务
     */
    private Integer taskType;

    /**
     * 任务状态
     */
    private Integer taskStatus;

    /**
     * 是否需要上传GPS位置
     */
    private Integer isRecordLocation;
}
