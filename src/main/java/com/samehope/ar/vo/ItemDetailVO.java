package com.samehope.ar.vo;

import lombok.Data;

import java.util.List;

/**
 * @Description: 巡检项视图对象
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class ItemDetailVO {

    private Long taskItemId;

    private String itemName;

    private String itemNo;

    private String itemDesc;

    private Integer isPhoto;

    private Integer isVideo;

    private Integer isRemark;

    private Integer subjectType;

    private String title;

    private Integer itemStatus;

    private List<ItemChildDetailVO> children;
}
