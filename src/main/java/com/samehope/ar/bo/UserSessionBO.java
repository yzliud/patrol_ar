package com.samehope.ar.bo;

import lombok.Data;

/**
 * @Description:
 * @Author: ZhangLuo
 * @Email: 1946430@qq.com
 */
@Data
public class UserSessionBO {

    private Long userId;

    private Long companyId;

    private Long projectId;

    private String mobile;

    private String ip;

    private String openId;

    private String name;

    private Long deviceId;

    private String deviceMac;
}
