package com.samehope.ar.bo;

import com.samehope.ar.entity.SysRole;
import com.samehope.ar.entity.SysUser;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * SpringSecurity需要的用户详情
 * Created by macro on 2018/4/26.
 */
public class CustomUserDetails implements UserDetails {

    private SysUser sysUser;
    private List<SysRole> roles;
    public CustomUserDetails(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    // TODO 权限值是什么鬼
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (roles == null) {
            throw new AccessDeniedException("没有相关权限");
        }
        //返回当前用户的权限
        return roles.stream()
                .filter(role -> role.getRoleName() != null)
                .map(role -> new SimpleGrantedAuthority(role.getRoleName()))
                .collect(Collectors.toList());
    }

    public void setRoles(List<SysRole> roles) {
        this.roles = roles;
    }

    public Long getUserId() {
        return sysUser.getId();
    }

    @Override
    public String getPassword() {
        return sysUser.getPwd();
    }

    @Override
    public String getUsername() {
        return sysUser.getMobile();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return 0 == sysUser.getDelFlag();
    }

    public SysUser getUser() {
        return sysUser;
    }
}
