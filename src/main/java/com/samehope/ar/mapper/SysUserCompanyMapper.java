package com.samehope.ar.mapper;

import com.samehope.ar.entity.SysUserCompany;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户公司 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface SysUserCompanyMapper extends BaseMapper<SysUserCompany> {

}
