package com.samehope.ar.mapper;

import com.samehope.ar.entity.DeviceRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 设备使用记录 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface DeviceRecordMapper extends BaseMapper<DeviceRecord> {

}
