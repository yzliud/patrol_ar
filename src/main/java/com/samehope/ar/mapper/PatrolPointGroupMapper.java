package com.samehope.ar.mapper;

import com.samehope.ar.entity.PatrolPointGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 巡检点分组表 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface PatrolPointGroupMapper extends BaseMapper<PatrolPointGroup> {

}
