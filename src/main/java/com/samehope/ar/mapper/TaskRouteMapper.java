package com.samehope.ar.mapper;

import com.samehope.ar.entity.TaskRoute;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 巡检路线快照 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface TaskRouteMapper extends BaseMapper<TaskRoute> {

}
