package com.samehope.ar.mapper;

import com.samehope.ar.entity.TaskItemChoice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 巡检项选项快照 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface TaskItemChoiceMapper extends BaseMapper<TaskItemChoice> {

}
