package com.samehope.ar.mapper;

import com.samehope.ar.entity.TaskItemChild;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 巡检项选项快照 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2020-01-14
 */
public interface TaskItemChildMapper extends BaseMapper<TaskItemChild> {

    /**
     * 根据巡检项和ID查询子项
     * @param taskItemId
     * @param taskItemChildId
     * @return
     */
    TaskItemChild findByItemAndChild(@Param("taskItemId") Long taskItemId,
                                     @Param("taskItemChildId") Long taskItemChildId);
}
