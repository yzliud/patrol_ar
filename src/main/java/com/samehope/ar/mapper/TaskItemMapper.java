package com.samehope.ar.mapper;

import com.samehope.ar.entity.TaskItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 巡检项快照 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface TaskItemMapper extends BaseMapper<TaskItem> {

    /**
     * 根据项目和ID查询巡检项
     * @param projectId
     * @param taskItemId
     * @return
     */
    TaskItem findByProjectAndTaskItem(@Param("projectId") Long projectId,
                                      @Param("taskItemId") Long taskItemId);
}
