package com.samehope.ar.mapper;

import com.samehope.ar.entity.TaskPoint;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.samehope.ar.vo.PointDetailVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 巡检点快照 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface TaskPointMapper extends BaseMapper<TaskPoint> {

    /**
     * 根据项目和任务ID获取巡检点明细
     * @param projectId
     * @param taskId
     * @return
     */
    List<PointDetailVO> findPointsByProjectAndTask(@Param("projectId") Long projectId,
                                                   @Param("taskId") Long taskId);

}
