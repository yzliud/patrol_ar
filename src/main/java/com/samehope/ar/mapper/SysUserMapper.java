package com.samehope.ar.mapper;

import com.samehope.ar.entity.SysRole;
import com.samehope.ar.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 根据手机号码查找用户
     * @param mobile
     * @return
     */
    SysUser findByMobile(String mobile);

    /**
     * 根据用户查找用户权限集合
     * @param userId
     * @param companyId
     * @param projectId
     * @return
     */
    List<SysRole> getRoleListByUserAndCompanyAndProject(@Param("userId") Long userId,
                                                        @Param("companyId") Long companyId,
                                                        @Param("projectId") Long projectId);
}
