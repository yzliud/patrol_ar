package com.samehope.ar.mapper;

import com.samehope.ar.entity.TaskItemReply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 巡检项回复 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface TaskItemReplyMapper extends BaseMapper<TaskItemReply> {

}
