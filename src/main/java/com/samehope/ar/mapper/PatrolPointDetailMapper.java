package com.samehope.ar.mapper;

import com.samehope.ar.entity.PatrolPointDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 巡检点项目明细 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface PatrolPointDetailMapper extends BaseMapper<PatrolPointDetail> {

}
