package com.samehope.ar.mapper;

import com.samehope.ar.entity.PatrolItemType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 巡检项分类表 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface PatrolItemTypeMapper extends BaseMapper<PatrolItemType> {

}
