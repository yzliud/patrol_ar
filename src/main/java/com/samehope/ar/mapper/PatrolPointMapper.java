package com.samehope.ar.mapper;

import com.samehope.ar.entity.PatrolPoint;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/**
 * <p>
 * 巡检点 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface PatrolPointMapper extends BaseMapper<PatrolPoint> {


}
