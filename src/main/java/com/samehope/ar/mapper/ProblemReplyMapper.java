package com.samehope.ar.mapper;

import com.samehope.ar.entity.ProblemReply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 问题处理 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ProblemReplyMapper extends BaseMapper<ProblemReply> {

}
