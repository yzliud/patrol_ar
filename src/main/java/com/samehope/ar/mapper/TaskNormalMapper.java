package com.samehope.ar.mapper;

import com.samehope.ar.entity.TaskNormal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.samehope.ar.vo.TaskListVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 正式巡检任务 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface TaskNormalMapper extends BaseMapper<TaskNormal> {

    /**
     * 团队成员获取任务
     * @param projectId
     * @return
     */
    List<TaskListVO> getTasksByProjectAndUser(@Param("projectId") Long projectId,
                                              @Param("userId") Long userId);

    /**
     * 当前待执行的任务
     * @param projectId
     * @param userId
     * @return
     */
    List<TaskListVO> getTasksInProgressByProjectAndUser(@Param("projectId") Long projectId,
                                                        @Param("userId") Long userId);
}
