package com.samehope.ar.mapper;

import com.samehope.ar.entity.Device;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 设备 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface DeviceMapper extends BaseMapper<Device> {

    /**
     * 根据设备识别码查找设备
     * @param deviceMac
     * @return
     */
    Device findByDeviceMac(String deviceMac);
}
