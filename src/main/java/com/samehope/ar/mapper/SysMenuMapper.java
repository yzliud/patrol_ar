package com.samehope.ar.mapper;

import com.samehope.ar.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜单 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
