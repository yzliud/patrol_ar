package com.samehope.ar.mapper;

import com.samehope.ar.entity.PatrolItemChild;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 巡检项子表 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface PatrolItemChildMapper extends BaseMapper<PatrolItemChild> {

}
