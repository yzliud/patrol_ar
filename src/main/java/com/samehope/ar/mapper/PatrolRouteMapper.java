package com.samehope.ar.mapper;

import com.samehope.ar.entity.PatrolRoute;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 巡检路线 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface PatrolRouteMapper extends BaseMapper<PatrolRoute> {

}
