package com.samehope.ar.mapper;

import com.samehope.ar.entity.SysDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

}
