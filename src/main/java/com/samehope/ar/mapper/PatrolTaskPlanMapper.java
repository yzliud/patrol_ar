package com.samehope.ar.mapper;

import com.samehope.ar.entity.PatrolTaskPlan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 巡检计划任务 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface PatrolTaskPlanMapper extends BaseMapper<PatrolTaskPlan> {

}
