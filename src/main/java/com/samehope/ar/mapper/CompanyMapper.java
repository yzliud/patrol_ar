package com.samehope.ar.mapper;

import com.samehope.ar.entity.Company;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 公司 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface CompanyMapper extends BaseMapper<Company> {

}
