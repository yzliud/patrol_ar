package com.samehope.ar.mapper;

import com.samehope.ar.entity.ProjectTeam;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 项目团队 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ProjectTeamMapper extends BaseMapper<ProjectTeam> {

}
