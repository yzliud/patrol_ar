package com.samehope.ar.mapper;

import com.samehope.ar.entity.Project;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.samehope.ar.vo.ProjectListVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 项目 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface ProjectMapper extends BaseMapper<Project> {

    /**
     * 分页获取所有项目
     * @param companyId
     * @return
     */
    List<ProjectListVO> getProjects(@Param("companyId") Long companyId,
                                    @Param("userId") Long userId);
}
