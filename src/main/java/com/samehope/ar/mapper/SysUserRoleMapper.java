package com.samehope.ar.mapper;

import com.samehope.ar.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户项目角色 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
