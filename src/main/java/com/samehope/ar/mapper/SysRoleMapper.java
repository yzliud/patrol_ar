package com.samehope.ar.mapper;

import com.samehope.ar.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author ZhangLuo
 * @since 2019-12-31
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
